package com.gildedrose;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

	private void updateQuality(Item item, int daysToWait, int expectedQuality) {
		GildedRose inn = new GildedRose(new Item[] {item});
		for(int days = 1; days <= daysToWait; days++)
			inn.updateQuality();
		assertEquals(expectedQuality, item.quality);
	}
	
	@Test
	public void theToStringMethod() {
		assertEquals("+5 Dexterity Vest, 10, 20", new Item("+5 Dexterity Vest", 10, 20).toString());
	}
	
	@Test
	public void DexterityVest_1Day() {
		updateQuality(new Item("+5 Dexterity Vest", 10, 20), 1, 19);
	}
	@Test
	public void DexterityVest_10Days() {
		updateQuality(new Item("+5 Dexterity Vest", 10, 20), 10, 10);
	}
	@Test
	public void AgedBrie_1Day() {
		updateQuality(new Item("Aged Brie", 2, 0), 1, 1);
	}
	@Test
	public void AgedBrie_5Days() {
		updateQuality(new Item("Aged Brie", 2, 0), 5, 8);
	}
	@Test
	public void AgedBrie_10Days() {
		updateQuality(new Item("Aged Brie", 2, 0), 10, 18);
	}
	@Test
	public void ElixirOfTheMongoose_1Day() {
		updateQuality(new Item("Elixir of the Mongoose", 5, 7), 1, 6);
	}
	@Test
	public void ElixirOfTheMongoose_10Days() {
		updateQuality(new Item("Elixir of the Mongoose", 5, 7), 10, 0);
	}
	@Test
	public void Sulfuras_1Day() {
		updateQuality(new Item("Sulfuras, Hand of Ragnaros", 0, 80), 1, 80);
	}
	@Test
	public void Sulfuras_10Days() {
		updateQuality(new Item("Sulfuras, Hand of Ragnaros", 0, 80), 10, 80);
	}
	@Test
	public void BackstagePasses_1Day() {
		updateQuality(new Item("Backstage passes to a TAFKAL80ETC concert", 5, 30), 1, 33);
	}
	@Test
	public void BackstagePasses_4Days() {
		updateQuality(new Item("Backstage passes to a TAFKAL80ETC concert", 5, 30), 4, 42);
	}
	@Test
	public void BackstagePasses_6Days() {
		updateQuality(new Item("Backstage passes to a TAFKAL80ETC concert", 12, 30), 6, 40);
	}
	@Test
	public void BackstagePasses_10Days() {
		updateQuality(new Item("Backstage passes to a TAFKAL80ETC concert", 5, 30), 10, 0);
	}
	@Test
	public void ConjuredManaPotion_1Day() {
		updateQuality(new Item("Conjured mana potion", 10, 30), 1, 28);
	}
	@Test
	public void ConjuredManaPotion_4Days() {
		updateQuality(new Item("Conjured mana potion", 10, 30), 4, 22);
	}
	@Test
	public void ConjuredManaPotion_10Days() {
		updateQuality(new Item("Conjured mana potion", 10, 30), 10, 10);
	}
}
