package com.gildedrose;

class GildedRose {
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item item : items) {
	    	if(item.name.equals("Sulfuras, Hand of Ragnaros")) {
	    		item.quality = 80;
	    	}
	    	else if(item.name.startsWith("Conjured")) {
	    		item.quality -= 2;
	    	}
	    	else if(item.name.startsWith("Backstage pass")) {
	    		if(item.sellIn > 10)
	    			item.quality++;
	    		else if(item.sellIn > 5)
	    			item.quality += 2;
	    		else if(item.sellIn >= 0)
	    			item.quality += 3;
	    		else
	    			item.quality = 0;
	    	}
	    	else if(item.name.equals("Aged Brie")) {
	    		if(item.sellIn <= 0)
	    			item.quality += 2;
	    		else
	    			item.quality++;
	    	}
	    	else {
	    		item.quality--;
	    	}
	    	
	    	if(item.quality < 0)
	    		item.quality = 0;
	    	item.sellIn--;
        }
    }
}